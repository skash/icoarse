# Code

Here is the code that you can modify and run on DaPony.

PreProcess* scripts need to be run from within the directory containing the functional runs. 

It looks for file name such as sub${s}_ep3d_0p8_func_run${r}.nii.gz

Use DemoRemoveBackgroundNoise.m from [JoseMarques's Github](https://github.com/JosePMarques/MP2RAGE-related-scripts.git) on T1w/UNI images.

Use my [docker image of PyPRF](https://gitlab.com/skash/docker_pyprf.git) to fit pRFs to the pre-processed data.


## Other stuff

Detach script from terminal using:

example:
`nohup bash PreProcessNew.sh > sub1_func_preproc.log &`

view log:
`tail -f sub1_func_preproc.log`