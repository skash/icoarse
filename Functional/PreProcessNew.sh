#! /bin/bash

# Style
bold=$(tput bold)
normal=$(tput sgr0)

# Init
n_threads=24
subjid=sub2
tr=2.046
nvols_func=312
nvols_func_ope=5
start_time=$(date +%s)
n_runs=3
export ANTS_RANDOM_SEED=3791
export OASIS=~/Documents/ANTs_Atlases

# Process Functional Data
## Preparation for SPM style registration
echo "${bold}Preparing to process ${subjid}${normal}"
for r in $(eval echo "{1..$n_runs}"); do

    if [ -d "$PWD/${subjid}_func_run${r}" ]; then

        echo "---- Directory ${subjid}_func_run${r} exists."

        if [ -f "$PWD/${subjid}_func_run${r}/vol_1303.nii.gz" ]; then

            echo "---- Disassembled timeseries data exists."

        else
            $ANTSPATH/ImageMath 4 \
                $PWD/${subjid}_func_run${r}/vol_.nii.gz \
                TimeSeriesDisassemble \
                $PWD/${subjid}_ep3d_0p8_func_run${r}.nii.gz

            echo "---- Directory ${subjid}_func_run${r} exists. Timeseries disassembled."
        fi
    else

        mkdir $PWD/${subjid}_func_run${r}
        $ANTSPATH/ImageMath 4 \
            $PWD/${subjid}_func_run${r}/vol_.nii.gz \
            TimeSeriesDisassemble \
            $PWD/${subjid}_ep3d_0p8_func_run${r}.nii.gz

        echo "---- Directory ${subjid}_func_run${r} created. Timeseries disassembled."

    fi

    if [ -d "$PWD/${subjid}_func_ope_run${r}" ]; then

        echo "---- Directory ${subjid}_func_ope_run${r} exists."

        if [ -f "$PWD/${subjid}_func_ope_run${r}/vol_1303.nii.gz" ]; then

            echo "---- Disassembled timeseries data exists."

        else
            $ANTSPATH/ImageMath 4 \
                $PWD/${subjid}_func_ope_run${r}/vol_.nii.gz \
                TimeSeriesDisassemble \
                $PWD/${subjid}_ep3d_0p8_func_ope_run${r}.nii.gz

            echo "---- Directory ${subjid}_func_ope_run${r} exists. Timeseries disassembled."
        fi
    else

        mkdir $PWD/${subjid}_func_ope_run${r}
        $ANTSPATH/ImageMath 4 \
            $PWD/${subjid}_func_ope_run${r}/vol_.nii.gz \
            TimeSeriesDisassemble \
            $PWD/${subjid}_ep3d_0p8_func_ope_run${r}.nii.gz

        echo "---- Directory ${subjid}_func_ope_run${r} created. Timeseries disassembled."

    fi

done

### Set registration parameters
INIT_PARAMS="--verbose 0 \
--dimensionality 3 \
--float 0 \
--random-seed ${ANTS_RANDOM_SEED} \
--use-estimate-learning-rate-once 1 \
--collapse-output-transforms 1 \
--interpolation BSpline[4] \
--use-histogram-matching 1 \
--winsorize-image-intensities [ 0.005,0.995 ]"
RIGID_PARAMS="--convergence [ 1000x500,1e-6,10 ] \
--shrink-factors 2x1 \
--smoothing-sigmas 1x0vox"
AFFINE_PARAMS="--convergence [ 500x250,1e-6,10 ] \
--shrink-factors 2x1 \
--smoothing-sigmas 1x0vox"

### Set multithreading preferences
ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=$n_threads
export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS

#### Make mask of fixed image
r=1 # first run
if [ -f "$PWD/${subjid}_func_run${r}_vol0_DEN.nii.gz" ]; then
    echo "---- Reference image and mask exists. Moving on."
else
    $ANTSPATH/DenoiseImage \
        --image-dimensionality 3 \
        --noise-model Rician \
        --patch-radius 1x1x1 \
        --input-image $PWD/${subjid}_func_run${r}/vol_1000.nii.gz \
        --output ${subjid}_func_run${r}_vol0_DEN.nii.gz

    $ANTSPATH/ThresholdImage 3 ${subjid}_func_run${r}_vol0_DEN.nii.gz ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz 250 1.e9
    $ANTSPATH/ImageMath 3 ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz ME ${subjid}_func_run1_vol0_DEN_BrainExtractionMask.nii.gz 1
    $ANTSPATH/ImageMath 3 ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz GetLargestComponent ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz
    $ANTSPATH/ImageMath 3 ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz MD ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz 2
    $ANTSPATH/ImageMath 3 ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz ME ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz 1

    echo "---- Automatic brain mask created for Run${r}."
fi

#### Align first volume of each run to each other
basevol=1000
echo "---- Begin deformable realignment using ${n_threads} threads."

for r in $(eval echo "{2..$n_runs}"); do
    ##### Set fixed, moving images and output
    FIXED_IM=$PWD/${subjid}_func_run1/vol_${basevol}.nii.gz
    FIXED_MSK=$PWD/${subjid}_func_run1_vol0_DEN_BrainExtractionMask.nii.gz
    MOVING_IM=$PWD/${subjid}_func_run${r}/vol_${basevol}.nii.gz
    OUTPUT=$PWD/${subjid}_func_run${r}/vol_${basevol}_stage0_

    $ANTSPATH/antsRegistration \
        ${INIT_PARAMS} \
        --output [ ${OUTPUT} , ${OUTPUT}Warped.nii.gz , 1] \
        --initial-moving-transform [ ${FIXED_IM} , ${MOVING_IM} , 1 ] \
        --masks [ ${FIXED_MSK} , 1 ] \
        --transform Rigid[ 0.1 ] \
        --metric MI[ ${FIXED_IM} , ${MOVING_IM} , 1 , 32 , Regular , 0.25 ] \
        ${RIGID_PARAMS} \
        --transform Affine[ 0.1 ] \
        --metric MI[ ${FIXED_IM} , ${MOVING_IM} , 1 , 32 , Regular , 0.25 ] \
        ${AFFINE_PARAMS}

done

echo "-------- Realignment Stage 0 completed."

#### Make mask of fixed image for other runs
for r in $(eval echo "{2..$n_runs}"); do
    if [ -f "$PWD/${subjid}_func_run${r}_vol0_DEN.nii.gz" ]; then
        echo "---- Reference image and mask exists. Moving on."
    else
        $ANTSPATH/DenoiseImage \
            --image-dimensionality 3 \
            --noise-model Rician \
            --patch-radius 1x1x1 \
            --input-image $PWD/${subjid}_func_run${r}/vol_1000_stage0_Warped.nii.gz \
            --output ${subjid}_func_run${r}_vol0_DEN.nii.gz

        $ANTSPATH/ThresholdImage 3 ${subjid}_func_run${r}_vol0_DEN.nii.gz ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz 250 1.e9
        $ANTSPATH/ImageMath 3 ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz ME ${subjid}_func_run1_vol0_DEN_BrainExtractionMask.nii.gz 1
        $ANTSPATH/ImageMath 3 ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz GetLargestComponent ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz
        $ANTSPATH/ImageMath 3 ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz MD ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz 2
        $ANTSPATH/ImageMath 3 ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz ME ${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz 1

        echo "---- Automatic brain mask created for Run${r}."
    fi
done

#### Align all volumes to the realigned first volumes
basevol=1000 # re-indexing for ANTs
lastvol=$(($basevol + ${nvols_func} - 1))
for r in $(eval echo "{1..$n_runs}"); do
    echo "---- Begin deformable realignment of Run${r} using ${n_threads} threads."
    if [ $r -eq "1" ]; then

        for n in $(eval echo "{$basevol..$lastvol}"); do

            ##### Set fixed, moving images and output
            FIXED_IM=$PWD/${subjid}_func_run${r}/vol_${basevol}.nii.gz
            FIXED_MSK=$PWD/${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz
            MOVING_IM=$PWD/${subjid}_func_run${r}/vol_${n}.nii.gz
            OUTPUT=$PWD/${subjid}_func_run${r}/vol_${n}_stage1_

            $ANTSPATH/antsRegistration \
                ${INIT_PARAMS} \
                --output [ ${OUTPUT} , ${OUTPUT}Warped.nii.gz , 1] \
                --initial-moving-transform [ ${FIXED_IM} , ${MOVING_IM} , 1 ] \
                --masks [ ${FIXED_MSK} , 1 ] \
                --transform Rigid[ 0.1 ] \
                --metric MI[ ${FIXED_IM} , ${MOVING_IM} , 1 , 32 , Regular , 0.25 ] \
                ${RIGID_PARAMS} \
                --transform Affine[ 0.1 ] \
                --metric MI[ ${FIXED_IM} , ${MOVING_IM} , 1 , 32 , Regular , 0.25 ] \
                ${AFFINE_PARAMS}

        done
    else
        for n in $(eval echo "{$basevol..$lastvol}"); do
            ##### Set fixed, moving images and output
            FIXED_IM=$PWD/${subjid}_func_run${r}/vol_${basevol}_stage0_Warped.nii.gz
            FIXED_MSK=$PWD/${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz
            MOVING_IM=$PWD/${subjid}_func_run${r}/vol_${n}.nii.gz
            OUTPUT=$PWD/${subjid}_func_run${r}/vol_${n}_stage1_

            $ANTSPATH/antsRegistration \
                ${INIT_PARAMS} \
                --output [ ${OUTPUT} , ${OUTPUT}Warped.nii.gz , 1] \
                --initial-moving-transform [ ${FIXED_IM} , ${MOVING_IM} , 1 ] \
                --masks [ ${FIXED_MSK} , 1 ] \
                --transform Rigid[ 0.1 ] \
                --metric MI[ ${FIXED_IM} , ${MOVING_IM} , 1 , 32 , Regular , 0.25 ] \
                ${RIGID_PARAMS} \
                --transform Affine[ 0.1 ] \
                --metric MI[ ${FIXED_IM} , ${MOVING_IM} , 1 , 32 , Regular , 0.25 ] \
                ${AFFINE_PARAMS}

        done
    fi

    echo "-------- Realignment stage1 completed for Run${r}."

done

#### Align oPE volumes to the realigned first volumes
basevol=1000 # re-indexing for ANTs
lastvol=$(($basevol + ${nvols_func_ope} - 1))
for r in $(eval echo "{1..$n_runs}"); do
    echo "---- Begin deformable realignment of oPE Run${r} using ${n_threads} threads."
    if [ $r -eq "1" ]; then

        for n in $(eval echo "{$basevol..$lastvol}"); do

            ##### Set fixed, moving images and output
            FIXED_IM=$PWD/${subjid}_func_run${r}/vol_${basevol}.nii.gz
            FIXED_MSK=$PWD/${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz
            MOVING_IM=$PWD/${subjid}_func_ope_run${r}/vol_${n}.nii.gz
            OUTPUT=$PWD/${subjid}_func_ope_run${r}/vol_${n}_stage1_

            $ANTSPATH/antsRegistration \
                ${INIT_PARAMS} \
                --output [ ${OUTPUT} , ${OUTPUT}Warped.nii.gz , 1] \
                --initial-moving-transform [ ${FIXED_IM} , ${MOVING_IM} , 1 ] \
                --masks [ ${FIXED_MSK} , 1 ] \
                --transform Rigid[ 0.1 ] \
                --metric MI[ ${FIXED_IM} , ${MOVING_IM} , 1 , 32 , Regular , 0.25 ] \
                ${RIGID_PARAMS}

        done
    else
        for n in $(eval echo "{$basevol..$lastvol}"); do
            ##### Set fixed, moving images and output
            FIXED_IM=$PWD/${subjid}_func_run${r}/vol_${basevol}_stage0_Warped.nii.gz
            FIXED_MSK=$PWD/${subjid}_func_run${r}_vol0_DEN_BrainExtractionMask.nii.gz
            MOVING_IM=$PWD/${subjid}_func_ope_run${r}/vol_${n}.nii.gz
            OUTPUT=$PWD/${subjid}_func_ope_run${r}/vol_${n}_stage1_

            $ANTSPATH/antsRegistration \
                ${INIT_PARAMS} \
                --output [ ${OUTPUT} , ${OUTPUT}Warped.nii.gz , 1] \
                --initial-moving-transform [ ${FIXED_IM} , ${MOVING_IM} , 1 ] \
                --masks [ ${FIXED_MSK} , 1 ] \
                --transform Rigid[ 0.1 ] \
                --metric MI[ ${FIXED_IM} , ${MOVING_IM} , 1 , 32 , Regular , 0.25 ] \
                ${RIGID_PARAMS}

        done
    fi

    echo "-------- Realignment stage1 completed for oPE Run${r}."

done

#### Create blip-up blip-down volumes for 3D distortion-correction
echo "---- Begin averaging volumes together."

for r in $(eval echo "{1..$n_runs}"); do
    $ANTSPATH/AverageImages \
        3 \
        $PWD/${subjid}_func_run${r}_avg_blip_up.nii.gz \
        0 \
        $PWD/${subjid}_func_run${r}/vol_1000_stage1_Warped.nii.gz \
        $PWD/${subjid}_func_run${r}/vol_1001_stage1_Warped.nii.gz \
        $PWD/${subjid}_func_run${r}/vol_1002_stage1_Warped.nii.gz \
        $PWD/${subjid}_func_run${r}/vol_1003_stage1_Warped.nii.gz \
        $PWD/${subjid}_func_run${r}/vol_1004_stage1_Warped.nii.gz

    $ANTSPATH/AverageImages \
        3 \
        $PWD/${subjid}_func_ope_run${r}_avg_blip_down.nii.gz \
        0 \
        $PWD/${subjid}_func_ope_run${r}/vol_1000_stage1_Warped.nii.gz \
        $PWD/${subjid}_func_ope_run${r}/vol_1001_stage1_Warped.nii.gz \
        $PWD/${subjid}_func_ope_run${r}/vol_1002_stage1_Warped.nii.gz \
        $PWD/${subjid}_func_ope_run${r}/vol_1003_stage1_Warped.nii.gz \
        $PWD/${subjid}_func_ope_run${r}/vol_1004_stage1_Warped.nii.gz

    $ANTSPATH/antsMultivariateTemplateConstruction2.sh \
        -d 3 \
        -i 3 \
        -k 1 \
        -f 4x2x1 \
        -s 2x1x0vox \
        -q 50x25x5 \
        -t SyN \
        -m CC \
        -c 2 \
        -j 32 \
        -o $PWD/${subjid}_func_run${r}_avg_distcorr_ $PWD/${subjid}_func_run${r}_avg_blip_up.nii.gz $PWD/${subjid}_func_ope_run${r}_avg_blip_down.nii.gz

    # Delete some fles
    rm $PWD/${subjid}_func_run${r}_avg_distcorr_${subjid}_func_ope_run${r}_avg_blip_down10GenericAffine.mat
    rm $PWD/${subjid}_func_run${r}_avg_distcorr_${subjid}_func_ope_run${r}_avg_blip_down11InverseWarp.nii.gz
    rm $PWD/${subjid}_func_run${r}_avg_distcorr_${subjid}_func_ope_run${r}_avg_blip_down11Warp.nii.gz
    rm $PWD/${subjid}_func_run${r}_avg_distcorr_${subjid}_func_ope_run${r}_avg_blip_up01InverseWarp.nii.gz
    rm $PWD/${subjid}_func_run${r}_avg_distcorr_template0GenericAffine.mat
    rm $PWD/${subjid}_func_run${r}_avg_distcorr_template0${subjid}_func_ope_run${r}_avg_blip_down1WarpedToTemplate.nii.gz
    rm $PWD/${subjid}_func_run${r}_avg_distcorr_template0${subjid}_func_run${r}_avg_blip_up0WarpedToTemplate.nii.gz
    rm $PWD/${subjid}_func_run${r}_avg_distcorr_template0warp.nii.gz
    rm $PWD/${subjid}_func_run${r}_avg_distcorr_templatewarplog.txt

done

echo "---- 3D distortion maps calculated."

# Apply run-wise motion+distortion warps and average across runs
basevol=1000 # re-indexing for ANTs
lastvol=$(($basevol + ${nvols_func} - 1))

for r in $(eval echo "{1..$n_runs}"); do
    echo "---- Begin correction of Run${r}."
    for n in $(eval echo "{$basevol..$lastvol}"); do
        $ANTSPATH/antsApplyTransforms \
            --dimensionality 3 \
            --verbose 1 \
            --input-image-type 0 \
            --interpolation BSpline[4] \
            --float \
            --reference-image $PWD/${subjid}_func_run${r}_avg_distcorr_template0.nii.gz \
            --input $PWD/${subjid}_func_run${r}/vol_${n}.nii.gz \
            --transform $PWD/${subjid}_func_run${r}_avg_distcorr_${subjid}_func_run${r}_avg_blip_up01Warp.nii.gz \
            --transform $PWD/${subjid}_func_run${r}_avg_distcorr_${subjid}_func_run${r}_avg_blip_up00GenericAffine.mat \
            --transform $PWD/${subjid}_func_run${r}/vol_${n}_stage1_0GenericAffine.mat \
            --output $PWD/${subjid}_func_run${r}/vol_${n}_stage2_Warped.nii.gz
    done
    echo "---- Run${r} Stage 2 correction complete."

    # Concatenate into file
    echo "---- Avengers assemble!"

    $ANTSPATH/ImageMath \
        4 \
        $PWD/${subjid}_ep3d_0p8_func_run${r}_mc_dc.nii.gz \
        TimeSeriesAssemble \
        $tr \
        0 \
        $PWD/${subjid}_func_run${r}/vol_*_stage2_Warped.nii.gz

    echo "---- ${subjid}_ep3d_0p8_func_run${r}_mc_dc.nii.gz is created."
done

# Save transforms
for r in $(eval echo "{1..$n_runs}"); do
    rm $PWD/${subjid}_func_run${r}/*.nii.gz
    tar -cf ${subjid}_func_run${r}_mc_transforms.tar ${subjid}_func_run${r}/
    rm -rf rm $PWD/${subjid}_func_run${r}
    rm -rf rm $PWD/${subjid}_func_ope_run${r}
done

pigz -9 *.tar

end_time=$(date +%s)
net_time=$(($end_time - $start_time))

echo "---- $(($net_time / 60)) min $(($net_time % 60)) sec"
